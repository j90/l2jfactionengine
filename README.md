L2FactionEngine

Creado por Jeriko90 .

Mods :
- Menu Options for desactive configs .
- StartingAdena
- Announce Online Players.
- Announce Faction Players.
- Announce Castle Lords.
- Announce Olimpiad Days.
- Duration Skill Config .
- Custom Spawn Zone.
- Expertise Penality
- Player Items to Spawn
- PVP - PK Custom Title
- Subclass Everywhere on Magister.
- New Character Title Custom
- Command .Buff (Buff List).
- Party Teleport Mod
- Animation PVP System Config.
- Custom Global Drop .

Events :
- Faction Engine
- TVT
- Kill The Boss Event
- Random Fight Event (4 Configurables Zones).
- DeathMatch Event.

Security : 
Anti-PHX
Anti-Bots Prevention Captcha.

NPC : 
- Party Teleport
- Siege Manager
- Faction Manager
- Buffer
- GK Global

Cooming Soon :
- CTF
- Clan War
- Hitman Event