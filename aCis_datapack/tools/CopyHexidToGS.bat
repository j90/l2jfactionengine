@echo off
title Copy Hexid to GS
:start
set server=C:\server
set hexid=hexid(server 1).txt
copy /V "%server%\login\%hexid%" "%server%\gameserver\config\hexid.txt"
if ERRORLEVEL 2 goto restart
if ERRORLEVEL 1 goto error
goto end
:restart
echo.
echo Great Job.
echo.
goto start
:error
echo.
echo Error.
echo.
:end
echo.
echo Great Job.
echo.
pause
