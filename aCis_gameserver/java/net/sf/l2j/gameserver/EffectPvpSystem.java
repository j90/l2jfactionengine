package net.sf.l2j.gameserver;

import net.sf.l2j.Config;
import net.sf.l2j.gameserver.network.clientpackets.Say2;
import net.sf.l2j.gameserver.network.serverpackets.CreatureSay;
import net.sf.l2j.gameserver.network.serverpackets.MagicSkillLaunched;
import net.sf.l2j.gameserver.network.serverpackets.MagicSkillUse;
import net.sf.l2j.gameserver.model.actor.L2Character;
import net.sf.l2j.gameserver.model.actor.instance.L2PcInstance;

public class EffectPvpSystem
{
 public void MagicSystem(L2PcInstance player)
 {
 player.broadcastPacket(new MagicSkillUse(player , Config.ANIMATION_EFFECT_SKILL, 1, 0, 0));
 player.broadcastPacket(new MagicSkillLaunched(player , Config.ANIMATION_EFFECT_SKILL, 1)); 
 }
 public void killerMsg(L2PcInstance player)
 {
        player.sendPacket(new CreatureSay(1, Say2.PARTYROOM_COMMANDER, "[Congrats]", " "+ player.getName() +" you kill a enemy"));
 }
}