package net.sf.l2j.gameserver.handler.voicedcommandhandlers;

import net.sf.l2j.Config;
import net.sf.l2j.gameserver.datatables.SkillTable;
import net.sf.l2j.gameserver.handler.IVoicedCommandHandler;
//import net.sf.l2j.gameserver.model.L2Skill;
import net.sf.l2j.gameserver.model.actor.instance.L2PcInstance;
//import net.sf.l2j.gameserver.network.serverpackets.MagicSkillUse;
import net.sf.l2j.gameserver.network.serverpackets.PlaySound;
//import net.sf.l2j.gameserver.network.serverpackets.InventoryUpdate;
//import net.sf.l2j.gameserver.network.serverpackets.StatusUpdate;

/*
 * Created By Dagger
 */
public class Buffs implements IVoicedCommandHandler
{
	    private static final String[] VOICED_COMMANDS = {"buffs"};
	    
	    @Override
		public boolean useVoicedCommand(String command, L2PcInstance activeChar, String target)
	    {
			    if (command.equalsIgnoreCase("buffs"))
			    	
			    {
			    	// flood protection
					@SuppressWarnings("unused")
					long currentTimeMillis = System.currentTimeMillis();
				      {
				    	  
				          if (((activeChar.isInCombat()) || (activeChar.getPvpFlag() > 0) || (activeChar.getKarma() > 0) || (activeChar.isInJail()) || (activeChar.isInOlympiadMode()) || (activeChar.isDead()) || (activeChar.inObserverMode()) || (activeChar.isFestivalParticipant())))
				          {
				            activeChar.sendMessage("No es posible usar este comando en tu estado actual.");
				            return false;
				          }
				        if ((activeChar.isInCombat()) || (activeChar.getPvpFlag() > 0))
				        {
				          activeChar.sendMessage("No es posible usar este comando en modo combate o en pvp.");
				          return false;
				        }
				        if (activeChar.isInJail())
				        {
				          activeChar.sendMessage("Sorry,you are in Jail!");
				          return false;
				        }
				        if (activeChar.isInOlympiadMode())
				        {
				          activeChar.sendMessage("Sorry,you are in the Olympiad now.");
				          return false;
				        }
				        if (activeChar.isInDuel())
				        {
				          activeChar.sendMessage("Sorry,you are in a duel!");
				          return false;
				        }
				        if (activeChar.isDead())
				        {
				          activeChar.sendMessage("Sorry,you are dead.");
				          return false;
				        }
				        if ((activeChar.getKarma() > 0) || (activeChar.getPvpFlag() > 0))
				        {
				          activeChar.sendMessage("Sorry,you are in combat.");
				          return false;
				        }
				        if (activeChar.inObserverMode())
				        {
				          activeChar.sendMessage("Sorry,you are in the observation mode.");
				        }
				        else if (activeChar.isFestivalParticipant())
				        {
				          activeChar.sendMessage("Sorry,you are in a festival.");
				          return false;
				        }
			    			 
					  if(Config.COMMAND_PLAYER_BUFFME)
							{
							
								if (activeChar.isMageClass())
								{
									PlaySound _snd = new PlaySound(1, "siege_victory", 0, 0, 0, 0, 0);
								    activeChar.sendPacket(_snd);
									for (int[] mageBuffs : Config.MAGE_BUFFME_LIST)
									{
										if (mageBuffs == null)
											continue;
										
										SkillTable.getInstance().getInfo(mageBuffs[0], mageBuffs[1]).getEffects(activeChar, activeChar);
									}
								}
								else
									for (int[] fighterBuffs : Config.FIGHTER_BUFFME_LIST)
									{
										PlaySound _snd = new PlaySound(1, "siege_victory", 0, 0, 0, 0, 0);
									    activeChar.sendPacket(_snd);
										if (fighterBuffs == null)
											continue;
										
										SkillTable.getInstance().getInfo(fighterBuffs[0], fighterBuffs[1]).getEffects(activeChar, activeChar);
									}
							}
 
			    }
			    }
				return false;
				
			}
			/*
             * Created By Dagger
             */
		@Override
		public String[] getVoicedCommandList()
		{
			return VOICED_COMMANDS;
		}
	}
