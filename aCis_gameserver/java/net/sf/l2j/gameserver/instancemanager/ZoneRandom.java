/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.sf.l2j.gameserver.instancemanager;

import java.util.logging.Logger;

import net.sf.l2j.commons.concurrent.ThreadPool;
import net.sf.l2j.commons.random.Rnd;

import net.sf.l2j.Config;
import net.sf.l2j.gameserver.model.Location;
import net.sf.l2j.gameserver.model.World;
import net.sf.l2j.gameserver.model.actor.instance.L2PcInstance;
import net.sf.l2j.gameserver.model.zone.ZoneId;
import net.sf.l2j.gameserver.util.Broadcast;

public class ZoneRandom implements Runnable
{
	private static final Logger _log = Logger.getLogger(ZoneRandom.class.getName());
	
	public int REFRESH = Config.REFRESH_TIME_ZONE;
	public int RANDOM_RANGE = Config.RANGE_SPAWN_CHAR;
	public Location CURRENT_ZONE = null;
	public int ZONE_CONT = 0;
	
	public int ZONES[][] =
	{
		// Primer Zona PVP
		{
			Config.ZONA_RANDOM_PVP_1_X,
			Config.ZONA_RANDOM_PVP_1_Y,
			Config.ZONA_RANDOM_PVP_1_Z
		},
		// Segunda Zona PVP
		{
			Config.ZONA_RANDOM_PVP_2_X,
			Config.ZONA_RANDOM_PVP_2_Y,
			Config.ZONA_RANDOM_PVP_2_Z
		},
		// Tercer Zona PVP
		{
			Config.ZONA_RANDOM_PVP_3_X,
			Config.ZONA_RANDOM_PVP_3_Y,
			Config.ZONA_RANDOM_PVP_3_Z
		},
		// Cuarta Zona PVP
		{
			Config.ZONA_RANDOM_PVP_4_X,
			Config.ZONA_RANDOM_PVP_4_Y,
			Config.ZONA_RANDOM_PVP_4_Z
		},
	};
	
	public ZoneRandom()
	{
		_log.info("ZoneRandom: Cargando Zonas...");
		_log.info("Se Han Generado 4 Zonas.");
		ThreadPool.scheduleAtFixedRate(this, 0, REFRESH * 1000 * 60);
	}
	
	@Override
	public void run()
	{
		Location location = getRandomZone();
		
		for (L2PcInstance player : World.getInstance().getPlayers())
		{
			if (player.isInsideZone(ZoneId.RANDOM_ZONE))
			{
				int getX = location.getX() + Rnd.get(-RANDOM_RANGE, RANDOM_RANGE);
				int getY = location.getY() + Rnd.get(-RANDOM_RANGE, RANDOM_RANGE);
				player.teleToLocation(getX, getY, location.getZ(), 20);
			}
			
			Broadcast.announceToOnlinePlayers("The pvp area was changed.");
			Broadcast.announceToOnlinePlayers("Next random zone pvp area will be change after " + REFRESH + " minute(s).");
		}
	}
	
	public Location getRandomZone()
	{
		ZONE_CONT++;
		if(ZONE_CONT > (ZONES.length -1))
			
		{
			ZONE_CONT = 0;
		}
		
		return CURRENT_ZONE = new Location(ZONES[ZONE_CONT][0], ZONES[ZONE_CONT][1], ZONES[ZONE_CONT][2]);
	}
	
	public Location getCurrentZone()
	{
		return CURRENT_ZONE;
	}
	
	public static ZoneRandom getInstance()
	{
		return SingletonHolder._instance;
	}
	
	private static class SingletonHolder
	{
		protected static final ZoneRandom _instance = new ZoneRandom();
	}
}