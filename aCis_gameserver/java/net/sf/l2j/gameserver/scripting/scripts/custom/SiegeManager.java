/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.sf.l2j.gameserver.scripting.scripts.custom;

import java.util.StringTokenizer;
import java.util.logging.Logger;

import net.sf.l2j.Config;
import net.sf.l2j.gameserver.instancemanager.CastleManager;
import net.sf.l2j.gameserver.model.actor.L2Npc;
import net.sf.l2j.gameserver.model.actor.instance.L2PcInstance;
import net.sf.l2j.gameserver.model.entity.Castle;
import net.sf.l2j.gameserver.model.olympiad.OlympiadManager;
import net.sf.l2j.gameserver.network.serverpackets.ActionFailed;
import net.sf.l2j.gameserver.network.serverpackets.SiegeInfo;
import net.sf.l2j.gameserver.scripting.Quest;

/**
 * @author Toxico
 */

public class SiegeManager extends Quest
{
	public static final Logger _log = Logger.getLogger(SiegeManager.class.getName());
	
	public static final int NPC = Config.NPC_SIEGE_MANAGER;
	
	public SiegeManager()
	{
		super(-1, "custom");
		
		addStartNpc(NPC);
		addFirstTalkId(NPC);
		addTalkId(NPC);
	}
	
	@Override
	public String onFirstTalk(L2Npc npc, L2PcInstance activeChar)
	{
		if (activeChar.getQuestState(getName()) == null)
		{
			newQuestState(activeChar);
		}
		else if ((activeChar.isInCombat()) || (activeChar.getPvpFlag() == 1) || (activeChar.getKarma() != 0) || (OlympiadManager.getInstance().isRegistered(activeChar)) || (activeChar.isDead() || activeChar.isFakeDeath()))
			return "SiegeManager-Blocked.htm";
		
		return "SiegeManager.htm";
	}
	
	@Override
	public String onAdvEvent(String command, L2Npc npc, L2PcInstance activeChar)
	{
		StringTokenizer stringTokenizer = new StringTokenizer(command);
		command = stringTokenizer.nextToken();
		
		if (command.equals("registerSiege"))
		{
			String castleName = stringTokenizer.nextToken();
			switch (castleName)
			{
				case "Aden":
					registerSiege(activeChar, 5);
					break;
				
				case "Giran":
					registerSiege(activeChar, 3);
					break;
				
				case "Goddard":
					registerSiege(activeChar, 7);
					break;
				
				case "Rune":
					registerSiege(activeChar, 8);
					break;
				
				case "Shuttgart":
					registerSiege(activeChar, 9);
					break;
				
				case "Oren":
					registerSiege(activeChar, 4);
					break;
				
				case "Dion":
					registerSiege(activeChar, 2);
					break;
				
				case "Gludio":
					registerSiege(activeChar, 1);
					break;
				
				case "Innadril":
					registerSiege(activeChar, 6);
					break;
			}
		}
		return null;
	}
	
	public void registerSiege(L2PcInstance activeChar, int castleId)
	{
		Castle castle = CastleManager.getInstance().getCastleById(castleId);
		if (castle != null)
			activeChar.sendPacket(new SiegeInfo(castle));
		else
			activeChar.sendPacket(ActionFailed.STATIC_PACKET);
	}
	
	public static void main(String[] args)
	{
		new SiegeManager();
	}
}