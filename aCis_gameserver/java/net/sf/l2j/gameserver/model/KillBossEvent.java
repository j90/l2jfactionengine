/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see &lt;http://www.gnu.org/licenses/&gt;.
 */
package net.sf.l2j.gameserver.model;

import java.util.logging.Logger;
import net.sf.l2j.commons.concurrent.ThreadPool;
import net.sf.l2j.commons.random.Rnd;

import net.sf.l2j.Config;
import net.sf.l2j.gameserver.datatables.NpcTable;
import net.sf.l2j.gameserver.datatables.SpawnTable;
import net.sf.l2j.gameserver.model.actor.L2Npc;
import net.sf.l2j.gameserver.model.actor.template.NpcTemplate;
import net.sf.l2j.gameserver.util.Broadcast;

/**
 * @author Tiago
 * 
 */
public class KillBossEvent
{
	protected static final Logger _log = Logger.getLogger(KillBossEvent.class.getName());
	private static int bossId;
	private static L2Npc _witchInst;
	private static int[] localization;
	private static boolean statusEvent = false;

	protected static void start(){

		try{

			System.out.println("Kill Boss Event List : " + Config.KBE_LIST_BOSS_ID.length);
			bossId = Config.KBE_LIST_BOSS_ID[Rnd.get(Config.KBE_LIST_BOSS_ID.length)];
			localization = Config.KBE_SPAWN[Rnd.get(Config.KBE_SPAWN.length)];

			NpcTemplate _bossNpc = NpcTable.getInstance().getTemplate(bossId);
			L2Spawn bossSpawn = new L2Spawn(_bossNpc);

			bossSpawn.setLoc(localization[0], localization[1],localization[2],0);
			bossSpawn.setRespawnState(false);
			SpawnTable.getInstance().addNewSpawn(bossSpawn, false);
			_witchInst = bossSpawn.doSpawn(false);

			for(int i = 0 ; i < Config.KBE_ANNOUNCE_START.length ; i++){
				Broadcast.announceToOnlinePlayers(Config.KBE_ANNOUNCE_START[i], true);
			}

			statusEvent = true;

			sleep(Config.KBE_DURATION);

			SpawnTable.getInstance().deleteSpawn(bossSpawn, false);
			_witchInst.getSpawn().setRespawnState(false);
			_witchInst.deleteMe();
			SpawnTable.getInstance().deleteSpawn(_witchInst.getSpawn(), false);

			for(int i = 0 ; i < Config.KBE_ANNOUNCE_END.length ; i++){
				Broadcast.announceToOnlinePlayers(Config.KBE_ANNOUNCE_END[i], true);
			}

			statusEvent = false;

		}catch(Exception e){
			_log.warning("[ERRO]: "+e.toString());
		}
	}

	public static int getBossSelected()
	{
		return bossId;
	}

	/**
	 * {@literal Init Thread}
	 * @author Tiago
	 */
	public static void init()
	{
		ThreadPool.scheduleAtFixedRate(new Runnable()
		{
			@Override
			public void run()
			{
				start();
			}
		}, Config.KBE_INTERVAL*1000*60, Config.KBE_INTERVAL*1000*60);
	}

	/**
	 * {@literal Check status from Event}
	 * @author Tiago
	 * @return Boolean
	 */
	public static Boolean statusKillTheBossEvent(){
		return KillBossEvent.statusEvent;
	}

	/**
	 * @author Tiago
	 * @param minutes  Integer minutes
	 */
	private static void sleep(int minutes){
		try
		{
			Thread.sleep(minutes * 1000 * 60);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

}