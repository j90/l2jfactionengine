/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.sf.l2j.gameserver.model.entity;

import net.sf.l2j.Config;
import net.sf.l2j.gameserver.model.Announcement;
import net.sf.l2j.commons.concurrent.ThreadPool;
import net.sf.l2j.gameserver.model.World;
import net.sf.l2j.gameserver.util.Broadcast;

/**
 * 
 * @author Debian
 * @Adaptado Jeriko90
 *
 */

@SuppressWarnings("unused")
public class AnnounceOnlinePlayers
{
    public static void getInstance()
    {
    	ThreadPool.scheduleAtFixedRate(new Runnable()
        {
            @Override
            @SuppressWarnings("synthetic-access")
            public void run()
            {
                Announce();
            }
        }, 0,Config.ANNOUNCE_ONLINE_PLAYERS_DELAY * 1000);
     }
    
    private static void Announce()
    {
    	int NumberofPlayers = World.getInstance().getPlayers().size();

        if (NumberofPlayers == 1)
        	Broadcast.announceToOnlinePlayers(NumberofPlayers + " player is online.");
        else
        	Broadcast.announceToOnlinePlayers(NumberofPlayers + " players are online.");
    }
}