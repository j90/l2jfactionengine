/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.sf.l2j.gameserver.model.actor.instance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.StringTokenizer;

import net.sf.l2j.commons.random.Rnd;

import net.sf.l2j.Config;
import net.sf.l2j.L2DatabaseFactory;
import net.sf.l2j.gameserver.ai.CtrlIntention;
import net.sf.l2j.gameserver.model.actor.template.NpcTemplate;
import net.sf.l2j.gameserver.network.serverpackets.ActionFailed;
import net.sf.l2j.gameserver.network.serverpackets.MyTargetSelected;
import net.sf.l2j.gameserver.network.serverpackets.NpcHtmlMessage;
import net.sf.l2j.gameserver.network.serverpackets.SocialAction;
import net.sf.l2j.gameserver.network.serverpackets.ValidateLocation;

/**
 * @author DaRkRaGe [L2JOneo]
 * @Adaptado por Jeriko90
 */

public class L2FactionInstance extends L2NpcInstance
{
	public L2FactionInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}
	
	@Override
	public void onBypassFeedback(L2PcInstance player, String command)
	{
		player.sendPacket(ActionFailed.STATIC_PACKET);
		StringTokenizer st = new StringTokenizer(command, " ");
		String actualCommand = st.nextToken();
		int val = 0;
		if (st.countTokens() >= 1)
		{
			val = Integer.valueOf(st.nextToken()).intValue();
		}
		
		if (actualCommand.equalsIgnoreCase("setffaction"))
		{
			if (player.isffaction())
			{
				player.sendMessage("You are allready a " + Config.FACTION_SYSTEM_FIRST_FACTION_NAME + " faction.");
				return;
			}
			{
				try (Connection con = L2DatabaseFactory.getInstance().getConnection();
				PreparedStatement statement = con.prepareStatement("UPDATE characters SET sfaction=1, ffaction=0 WHERE obj_id=?"))
				{
					statement.setInt(1, player.getObjectId());
					statement.execute();
				}
				catch (Exception e)
				{
					_log.warning("Could not set sfaction status of char:");
				}
				System.out.println("Faction Engine : Player " + player.getName() + " has choose " + Config.FACTION_SYSTEM_FIRST_FACTION_NAME + " faction");
				player.broadcastUserInfo();
				if(Config.FACTION_AURA_TEAM_ENABLE)
				{
					player.setTeam(1);
				}
				player.setffaction(true);
				player.broadcastUserInfo();
				player.sendMessage("You are fighiting now for " + Config.FACTION_SYSTEM_FIRST_FACTION_NAME + " faction ");
				player.getAppearance().setNameColor(Config.FACTION_SYSTEM_FIRST_FACTION_NAME_COLOR);
				player.setTitle(Config.FACTION_SYSTEM_FIRST_FACTION_NAME);
				player.getAppearance().setTitleColor(Config.FACTION_SYSTEM_FIRST_FACTION_TITLE_COLOR);
				player.teleToLocation(Config.FACTION_SYSTEM_FIRST_FACTION_BASE_SPAWN_X, Config.FACTION_SYSTEM_FIRST_FACTION_BASE_SPAWN_Y, Config.FACTION_SYSTEM_FIRST_FACTION_BASE_SPAWN_Z, 0);
			}
		}
		
		if (actualCommand.equalsIgnoreCase("setsfaction"))
		{
			if (player.issfaction())
			{
				player.sendMessage("You are allready a " + Config.FACTION_SYSTEM_SECOND_FACTION_NAME + " faction.");
				return;
			}
			{
				try (Connection con = L2DatabaseFactory.getInstance().getConnection();
				PreparedStatement statement = con.prepareStatement("UPDATE characters SET sfaction=0, ffaction=1 WHERE obj_id=?"))
				{
					statement.setInt(1, player.getObjectId());
					statement.execute();
				}
				catch (Exception e)
				{
					_log.warning("Could not set ffaction status of char:");
				}
				System.out.println("Faction Engine : Player " + player.getName() + " has choose " + Config.FACTION_SYSTEM_SECOND_FACTION_NAME + " faction");
					
				player.broadcastUserInfo();
				if(Config.FACTION_AURA_TEAM_ENABLE)
				{
					player.setTeam(2);
				}
				player.setsfaction(true);
				player.broadcastUserInfo();
				player.sendMessage("You are fighiting now for " + Config.FACTION_SYSTEM_SECOND_FACTION_NAME + " faction ");
				player.getAppearance().setNameColor(Config.FACTION_SYSTEM_SECOND_FACTION_NAME_COLOR);
				player.setTitle(Config.FACTION_SYSTEM_SECOND_FACTION_NAME);
				player.getAppearance().setTitleColor(Config.FACTION_SYSTEM_SECOND_FACTION_TITLE_COLOR);
				player.teleToLocation(Config.FACTION_SYSTEM_SECOND_FACTION_BASE_SPAWN_X, Config.FACTION_SYSTEM_SECOND_FACTION_BASE_SPAWN_Y, Config.FACTION_SYSTEM_SECOND_FACTION_BASE_SPAWN_Z, 0);
					
			}
		}
		else
		{
			super.onBypassFeedback(player, command);
		}
	}
	
	@Override
	public void onAction(L2PcInstance player)
	{
		if (this != player.getTarget())
		{
			player.setTarget(this);
			player.sendPacket(new MyTargetSelected(getObjectId(), player.getLevel() - getLevel()));
			player.sendPacket(new ValidateLocation(this));
		}
		else if (isInsideRadius(player, INTERACTION_DISTANCE, false, false))
		{
			SocialAction sa = new SocialAction(player, Rnd.get(8));
			broadcastPacket(sa);
			player.setCurrentFolkNPC(this);
			showMessageWindow(player);
			player.sendPacket(ActionFailed.STATIC_PACKET);
		}
		else
		{
			player.getAI().setIntention(CtrlIntention.INTERACT, this);
			player.sendPacket(ActionFailed.STATIC_PACKET);
		}
	}
	
	private void showMessageWindow(L2PcInstance player)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(1);
		html.setFile("data/html/mods/faction/faction.htm");
		html.replace("%objectId%", String.valueOf(getObjectId()));
		html.replace("%npcname%", getName());
		player.sendPacket(html);
	}
}